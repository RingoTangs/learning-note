# 一、Running Replicated ZooKeeper

## 1. Running multiple servers on a single machine

1. [官网](https://zookeeper.apache.org/doc/current/zookeeperStarted.html#sc_RunningReplicatedZooKeeper)
2. [配置参考](https://juejin.cn/post/6844903609520373767)

首先在 `Linux` 服务器上准备3个 `ZooKeepr`。
```shell
drwxr-xr-x 7 root root     146 Jan 21 20:41 zookeeper-3.5.7-1
drwxr-xr-x 7 root root     146 Jan 21 21:52 zookeeper-3.5.7-2
drwxr-xr-x 7 root root     146 Jan 21 21:52 zookeeper-3.5.7-3
```

分别在以上3个的 `conf/` 目录下添加 `zoo.cfg`。

```properties
# zookeeper-3.5.7-1 的 zoo.cfg
tickTime=2000
dataDir=/var/lib/zookeeper/1
clientPort=2181
initLimit=5
syncLimit=2
server.1=localhost:2888:3888
server.2=localhost:2889:3889
server.3=localhost:2890:3890

# zookeeper-3.5.7-2 的 zoo.cfg
tickTime=2000
dataDir=/var/lib/zookeeper/2
clientPort=2182
initLimit=5
syncLimit=2
server.1=localhost:2888:3888
server.2=localhost:2889:3889
server.3=localhost:2890:3890

# zookeeper-3.5.7-3 的 zoo.cfg
tickTime=2000
dataDir=/var/lib/zookeeper/3
clientPort=2183
initLimit=5
syncLimit=2
server.1=localhost:2888:3888
server.2=localhost:2889:3889
server.3=localhost:2890:3890
```

然后分别在3个 `dataDir` 目录下创建 `myid` 文件。

```properties
# /var/lib/zookeeper/1 目录下的 myid 文件
1

# /var/lib/zookeeper/2 目录下的 myid 文件
2

# /var/lib/zookeeper/3 目录下的 myid 文件
3
```

最后分别到 `ZooKeeper` 的 `bin` 目录下启动即可

```shell
# 启动命令
./zkServer start
```

## 2. 构建zookeeper镜像

`DOckerfile` 文件

```dockerfile
FROM centos:7
MAINTAINER Ringo

# 创建目录
RUN mkdir -p /usr/local/java
RUN mkdir -p /zookeeper

# java 和 zookeeper 加入到容器
ADD jdk-8u251-linux-x64.tar.gz /usr/local/java
ADD apache-zookeeper-3.5.7-bin.tar.gz /zookeeper

RUN mv /zookeeper/apache-zookeeper-3.5.7-bin/ /zookeeper/zookeeper-3.5.7

# 配置 java 环境变量
ENV JAVA_HOME=/usr/local/java/jdk1.8.0_251
ENV JRE_HOME=${JAVA_HOME}/jre
ENV CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
ENV PATH=${JAVA_HOME}/bin:$PATH


# 暴露路径
# zoo.cfg 的路径
VOLUME /zookeeper/zookeeper-3.5.7/conf
# dataDir 的路径(zoo.cfg中要配置这个路径)
VOLUME /var/lib/zookeeper

# 暴露端口
EXPOSE 2888 3888 2181
```

构建镜像

```shell
docker build -t zookeeper:v4 .
```

单机版本 `zookeeper`

```shell
# 启动命令
docker run -it --name zk -p 2888:2888 -p 3888:3888 -p 2181:2181 \
-v /root/zks/zoo.cfg:/zookeeper/zookeeper-3.5.7/conf/zoo.cfg \
-v /var/lib/zookeeper:/var/lib/zookeeper \
-d zookeeper:v4
```

```properties
# zoo.cfg 文件
tickTime=2000
dataDir=/var/lib/zookeeper
clientPort=2181
```



## 3. docker容器构建zookeeper集群

```shell
# 创建 docker 网络
docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 zkNet
```

3个zookeeper 的 `zoo.cfg` 配置文件相同。

```properties
tickTime=2000
dataDir=/var/lib/zookeeper
clientPort=2181
initLimit=5
syncLimit=2
server.1=192.168.0.3:2888:3888
server.2=192.168.0.4:2888:3888
server.3=192.168.0.5:2888:3888
```

需要分别为 3 个 zookeeper 的 dataDir 添加 `myid` 文件。

```properties
# /root/zks/zk1/dataDir 的 myid 文件
1
# /root/zks/zk2/dataDir 的 myid 文件
2
# /root/zks/zk3/dataDir 的 myid 文件
3
```



启动 zookeeper 容器

```shell
# 启动 zk1 容器
docker run -it --name zk1 -p 2888:2888 -p 3888:3888 -p 2181:2181 \
-v /root/zks/zk1/conf/zoo.cfg:/zookeeper/zookeeper-3.5.7/conf/zoo.cfg \
-v /root/zks/zk1/dataDir:/var/lib/zookeeper \
--network zkNet --ip 192.168.0.3 \
-d zookeeper:v4

# 启动 zk2 容器
docker run -it --name zk2 -p 2889:2888 -p 3889:3888 -p 2182:2181 \
-v /root/zks/zk2/conf/zoo.cfg:/zookeeper/zookeeper-3.5.7/conf/zoo.cfg \
-v /root/zks/zk2/dataDir:/var/lib/zookeeper \
--network zkNet --ip 192.168.0.4 \
-d zookeeper:v4

# 启动 zk3 容器
docker run -it --name zk3 -p 2890:2888 -p 3890:3888 -p 2183:2181 \
-v /root/zks/zk3/conf/zoo.cfg:/zookeeper/zookeeper-3.5.7/conf/zoo.cfg \
-v /root/zks/zk3/dataDir:/var/lib/zookeeper \
--network zkNet --ip 192.168.0.5 \
-d zookeeper:v4
```

然后分别在容器中启动 zookeeper 服务

```shell
docker exec -it zk1 bash
docker exec -it zk2 bash
docker exec -it zk3 bash

bin/zkServer.sh start
```



