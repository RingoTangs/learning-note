# 一、Shell入门

## 1.1 Shell格式

Shell格式：以 `#!/bin/bash` 开头，作用是指定解析器。



## 1.2 执行Shell

```sh
# 第一种方式: 指定解析器
sh ./hello.sh
bash ./hello.sh

# 第二种方式（常用）：
chomd +x ./hello.sh
./hello.sh

# 第三种方式：
. ./hello.sh
source ./hello.sh
```

Notes：

- 第一种方式和第二种方式都是在当前 shell 的子 shell 中执行脚本。
- 第三种方式是在当前 shell 中执行脚本。
- 子 shell 不能访问父 shell 中的局部变量，具有隔离的作用。



# 二、变量

## 2.1 系统预定义变量

常用系统变量（全局变量）：`$HOME、$PWD、$SHELL、$USER、$PATH`。

**系统定义的环境变量的特点：都是由大写字母组成**！

```shell
# 查看系统变量的值
echo $HOME

# 带有$就可以当作变量使用，例如：
ls $HOME # 查看 HOME 目录下的文件

# 显示当前 Shell 中所有的变量：
set
```



## 2.2 自定义变量

**基本语法**：

1. 定义变量：变量名=变量值，**注意，=前后不能有空格**。
2. 撤销变量：`unset 变量名`。
3. 声名只读变量：`readonly 变量`，注意：不能 unset。
4. 自定义的变量属于当前 shell 中的变量，在子 shell 中不能访问。将局部变量提升为全局变量，需要使用 `export` 命令。 
5. 在 shell 中，变量默认都是字符串类型，无法直接进行数值运算。
6. 变量的值如果有空格，需要使用双引号或者单引号。



> 使用 `ps -f` 就可以查看 shell 之间的父子继承关系。



## 2.3 特殊变量

### 2.3.1 $n

$n: n为数字，$0 代表该脚本名称。$1-$9 代表第一到第九参数。

10以上的参数需要用大括号包含,例如：${10}。

案例如下：

```sh
#!/bin/bash

# parameter.sh

echo '=================$n=================' # 单引号直接输出字符串, 双引号会将 $n 解析为变量
echo script name: $0
echo 1st parameter: $1
echo 2st parameter: $2
```

```sh
[root@Lincy shell-demo]# ./parameter.sh 1 2
=================$n=================
script name: ./parameter.sh
1st parameter: 1
2st parameter: 2
```



### 2.3.2 $#

`$#`：获取所有输入参数个数，常用于循环，判断参数的个数是否正确以及加强脚本的健壮性。

```sh
#!/bin/bash

# parameter.sh

echo parameter number: $#
```

```sh
[root@Lincy shell-demo]# ./parameter.sh 1 2
parameter number: 2
```



### 2.3.3 $*、$@

`$*` 代表命令行中所有的参数，但是将所有参数看成一个整体。

`$@` 代表命令行中所有的参数，但是把每个参数区分对待（用于循环遍历）。



# 三、运算符 

基本语法：`$((expression))` 或者 `$[expression]`。

```shell
# 计算 (2 + 3) * 4 的值
[root@Lincy shell-demo]# result=$[(2+3)*4]
[root@Lincy shell-demo]# echo $result
20
```



# 四、条件判断

基本语法（以下二选一）：

1. `test condition`。
2. `[ condition ]`  ，注意 condition 前后要有空格，并且表达式字符之前也要有空格。

```sh
# 例子: 0 表示 True; 非 0 标识 False 
[root@Lincy shell-demo]# a=hello
[root@Lincy shell-demo]# [ $a = hello ]
[root@Lincy shell-demo]# echo $?
0
[root@Lincy shell-demo]# [ $a = Hello ]
[root@Lincy shell-demo]# echo $?
1
```



注意：如果是字符串之间的比较，用等号 `=` 判断相等。用 `!=` 判断不等。



两个整数之间比较：

- `-eq`：等于。`-ne`：不等于。
- `-lt `：小于。`-le`：小于等于。
- `-gt`: 大于。`-ge`：大于等于。

```sh
# 例子
[root@Lincy shell-demo]# [ 2 -lt 8 ]
[root@Lincy shell-demo]# echo $?
0
```





按照文件权限进行判断：

- `-r` 有读的权限（read）。
- `-w` 有写的权限（write）。
- `-x` 有执行的权限（execute）。

```sh
# 例子
[root@Lincy shell-demo]# [ -x 1.txt ]
[root@Lincy shell-demo]# echo $?
1
```



按照文件类型进行判断：

- `-e` 文件存在（existence）。
- `-f` 文件存在并且是一个常规的文件（file）。
- `-d` 文件存在并且是一个目录（directory）。

```sh
[root@Lincy shell-demo]# [ -e 2.txt ]
[root@Lincy shell-demo]# echo $?
1
[root@Lincy shell-demo]# [ -f 1.txt ]
[root@Lincy shell-demo]# echo $?
0
[root@Lincy shell-demo]# [ -d 1.txt ]
[root@Lincy shell-demo]# echo $?
1
```



多条件判断：`&&` 表示前一条命令执行成功时，才执行后一条命令，`||` 表示上一条命令执行失败后，才执行下一条命令（类似于三目运算符）。

```sh
[root@Lincy shell-demo]# [ -x 1.txt ] && echo 可执行 || echo 不可执行
不可执行
```



# 五、流程控制（重点）

 ## 5.1 if 判断

基本语法：

（1）单分支

```sh
# 1: 单分支
if [ expression ];then   # 分号的作用是用于分隔两行命令
	...
fi
```

或者

```sh
# 1: 单分支
if [ expression ]
then
	...
fi
```



案例：

```sh
[root@Lincy shell-demo]# a=25
[root@Lincy shell-demo]# if [ $a -gt 18 ] && [ $a -lt 35 ]; then echo OK; fi
OK
# -a: and 逻辑与
# -o: or 逻辑或
[root@Lincy shell-demo]# if [ $a -gt 18 -a $a -lt 35 ]; then echo OK; fi
OK
```



（2）多分支

```sh
# 2: 多分支
if [ expression ]
then
	...
elif [ expression ]
then
	...
else
	...
fi
```



## 5.2 case 语句

（1）基本语法

```sh
case $变量名 in
"值1")
	...
;;
"值2")
	...
;;	
*)
	如果变量的值都不是以上的值, 执行这段代码
;;
esac
```

注意：

- `case` 行尾必须为单词 "in"，每一个模式匹配必须以右括号 `)` 结束。
- 双分号 `;;` 表示命令序列结束，相当于 Java 中的 `break`。
- 最后的 `*)` 表示默认模式，相当于 Java 中的 `default`。

（2）案例

```sh
#!/bin/bash

# hello.sh

a=$1

case $a in
5)
        echo 5
;;
6)
        echo 6
;;
*)
        echo default 0
;;
esac
```

测试：

```shell
[root@Lincy shell-demo]# ./hello.sh 7
default 0
[root@Lincy shell-demo]# ./hello.sh 5
5
```



## 5.2 for 循环

（1）基本语法1

```sh
for (( 初始值; 循环控制条件; 变量变化 ))
do
	...
done
```

案例：

```sh
#!/bin/bash

for (( i = 0; i <= 100; i++ ))
do
	echo $i
done
```



（2）基本语法2

```sh
for 变量 in 值1 值2 值3...
do
	...
done	
```

案例：

```shell
[root@Lincy shell-demo]# for os in linux windows macos; do echo $os; done
linux
windows
macos

# {1..5} 序列生成式
[root@Lincy shell-demo]# for i in {1..5}; do echo $i; done
1
2
3
4
5
```



（3）$* 和 $@ 的区别

```sh
#!/bin/bash

# hello.sh

# $* 和 $@ 不用双引号引起来没有任何区别
echo '====================$*===================='
for i in "$*"; do echo $i; done

echo '====================$@===================='
for i in "$@"
do
        echo $i
done
```

测试结果：

```sh
[root@Lincy shell-demo]# ./hello.sh 1 2 3
====================$*====================
1 2 3
====================$@====================
1
2
3
```



## 5.3 while 循环

基本语法：

```sh
while [ expression ]
do
	...
done
```

案例：

```sh
[root@Lincy shell-demo]# a=0
[root@Lincy shell-demo]# while [ $a -le 10 ]; do echo $a; a=$[$a + 1]; done
0
1
2
3
4
5
6
7
8
9
10


# shell 中的 let 语法
[root@Lincy shell-demo]# a=0
[root@Lincy shell-demo]# while [ $a -le 10 ]; do echo $a; let a+=1; done
0
1
2
3
4
5
6
7
8
9
10
```









