// 1. 定义接口
function printLabel(labeledObj) {
    console.log(labeledObj.name);
    console.log(labeledObj.label);
}
printLabel({ name: 'zs', label: '123' });
function createSquare(config) {
    return {
        color: config.color ? config.color : 'black',
        area: config.width * 10
    };
}
// 这里传参就可以只传一个
const square = createSquare({ width: 10 });
console.log(square);
const p1 = { x: 1.5, y: 2.3 };
//  p1.x = 1.6 这行代码会报错
console.log('p1 =', p1);
// TypeScript 具有ReadonlyArray<T>类型，它与Array<T>相似，只是把所有可变方法去掉了，因此可以确保数组创建后再也不能被修改：
let a = [1, 2, 3, 4];
let ro = a;
// ro[0] = 12  // error
// ro.push(5)  // error
// a = ro      // error 也不能直接赋值
// 但是 ReadonlyArray 经过类型转换后就可以了
a = ro;
console.log('a =', a);
a = ro;
console.log('a =', a);
