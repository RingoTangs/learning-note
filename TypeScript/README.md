# 一、TS基础

## 1. 基础类型

`JavaScript` 中的基础数据类型。

- 原始类型：`string、number、boolean、null、undefined、bigint、symbol`。
- 包装类型：`String、Number、Boolean、BigInt、Symbol`。包装类型的 `typeof` 结果是 `object`。

问题：从内存来看 `null` 和 `undefined` 本质的区别是什么？

答：值 null 是一个字面量，它不像 undefined 是全局对象的一个属性。null 是指变量未指向任何对象，所以常用来将对象置空，释放对象的内存。但是它是存在的，只不过没有类型和值。

undefined 是全局对象的一个属性。一个没有被赋值的变量的类型是 undefined 。



