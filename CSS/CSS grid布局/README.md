# 一、容器的几个重要属性
## 1.1. display

```css
/* 
    display: grid;         块级元素的网格布局         
    display: inline-grid;  行内元素的网格布局
    相同点：容器内元素都会进行网格布局。
    不同点：
		display: grid;将容器作为块级元素。
		display: inline-grid;将容器作为行内元素。
*/
```



## 1.2. 定义行和列

```css
/* 定义 grid 布局 */
display: grid;
/* 定义 3 * 3 的网格 */
grid-template-columns: 100px 100px 100px;
grid-template-rows: 100px 100px 100px;
```



定义行和列的值：

- 可以是具体的像素值。
- 可以是百分比。
- 还可以是 `fr` 关键字。
- 也可以是 `auto`。

```css
/* 两列的宽度比例 => 1:2:1 */
grid-template-columns: 1fr 2fr 1fr;
```



## 1.3. repeat()

```css
/* repeat(n,value): 重复 n 次, 每 行/列 的宽度都是value */
grid-template-rows: repeat(3, 100px);



/* 
	repeat(n, value...) 重复 50px 100px 行/列 3次.
		n: 重复的次数。
		value: 模式。
*/
grid-template-rows: repeat(3, 50px 100px);
```



## 1.4. 定义行或列的间距

```css
/* 定义行列的间距 */
grid-row-gap: 10px;
grid-column-gap: 10px;
```

以上属性需要写在容器中。



## 1.5. 设置单元格与列/行轴对齐

```css
/* 单元格对于网格线的对齐方式 */
align-items: center;
justify-items: center;
```

以上属性需要写在容器中。





## 1.6. 整个内容区域在容器中的对齐方式

```css
/* 整个内容区域对于容器的对齐方式 */
align-content: center;
justify-content: center;
```

以上属性需要写在容器中。      



## 1.6. 给网格布局指定区域

```css
grid-template-areas:
"a b c"
"d e f"
"g h i";
```

以上属性需要写在容器中。



# 二、容器内项目的属性

## 2.1. 控制项目的位置

方式一：通过网格线。

```css
/* 
第一种方法：可以通过网格线的编号设置
*/
.item:nth-child(1) {
    background-color: lightseagreen;
    grid-row-start: 2;
    grid-row-end: 3;
    grid-column-start: 2;
    grid-column-end: 3;
}
```

方式二：通过给项目自定义名字。

```css
.container {
  display: grid;
  grid-template-rows: repeat(3, 200px);
  grid-template-columns: repeat(3, 200px);
  width: 600px;
  height: 600px;
  border: 1px solid red;
  margin: 100px auto;

  /* 给项目定义名字 */
  grid-template-areas:
    "a b c"
    "d e f"
    "g h i";
}

.item:nth-child(1) {
  background-color: pink;
  grid-row-start: e-start;
  grid-row-end: e-end;
  grid-column-start: e-start;
  grid-column-end: e-end;
}
```

`grid-row-start` 这些属性可以简写：

```css
.item:nth-child(1) {
  background-color: pink;
  grid-row: e-start/e-end;
  grid-column: e-start/e-end;
}
```



## 2.2. grid-area

```css
.container {
  display: grid;
  grid-template-rows: repeat(3, 200px);
  grid-template-columns: repeat(3, 200px);
  width: 600px;
  height: 600px;
  border: 1px solid red;
  margin: 100px auto;

  grid-template-areas:
    "a b c"
    "d e f"
    "g h i";
}

.item:nth-child(1) {
    background-color: pink;
    grid-area: e;
}
```

这样写比 2.1 更简单。