package org.github.project.strategy;

/**
 * @auther yang
 * @date 2022/1/21 12:29
 */
public enum FileTypeEnum {
    HTML, TXT, XML
}
