package org.github.project;

import org.github.project.chain.AbstractHandler;
import org.github.project.chain.OrderHandlerChain;
import org.github.project.observer.LoginStateEnum;
import org.github.project.observer.LoginStateService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @auther yang
 * @date 2022/1/21 11:10
 */
@SpringBootApplication
public class MainApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(MainApplication.class, args);

        System.out.println("========= 责任链模式 =========");
        AbstractHandler chain = applicationContext.getBean(OrderHandlerChain.class).getChain();
        chain.handle(null, null);

        System.out.println("========= 观察者模式 =========");
        LoginStateService loginStateService = applicationContext.getBean(LoginStateService.class);
        loginStateService.setState(LoginStateEnum.LOGIN);
        loginStateService.setState(LoginStateEnum.LOGOUT);
    }
}
