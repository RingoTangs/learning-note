package org.github.project.observer;

/**
 * 登录状态枚举
 *
 * @auther yang
 * @date 2022/1/21 13:02
 */
public enum LoginStateEnum {

    LOGIN(1, "登录"), LOGOUT(0, "退出");

    private int state;

    private String msg;

    LoginStateEnum(int state, String msg) {
        this.state = state;
        this.msg = msg;
    }

    public int getState() {
        return state;
    }

    public String getMsg() {
        return msg;
    }
}
