package org.github.project.chain;

/**
 * 责任链模式
 * 业务场景：下订单 ==> 参数非空校验 ==> 安全校验 ==> 黑名单校验 ==> 拦截规则等..
 * <p>
 * 可以通过 if 判断 + 异常来判断
 *
 * <pre>
 *     public class Order {
 *         public void checkNullParam(Object param) {
 *             // 参数非空校验
 *             throw new RuntimeException();
 *         }
 *         public void checkSecurity() {
 *             // 安全校验
 *             throw new RuntimeException();
 *         }
 *         public void checkBackList() {
 *             // 黑名单校验
 *             throw new RuntimeException();
 *         }
 *         public void checkRule() {
 *             // 规则拦截
 *             throw new RuntimeException();
 *         }
 *
 *         public static void main(String[] args) {
 *             Order order = new Order();
 *             try {
 *                 order.checkNullParam();
 *                 order.checkSecurity();
 *                 order.checkBackList();
 *                 order.checkRule();
 *             } catch (RuntimeException e) {
 *                 System.out.println("order fail");
 *             }
 *         }
 *     }
 * </pre>
 *
 * @auther yang
 * @date 2022/1/21 10:45
 */
public abstract class AbstractHandler {

    // 责任链中的下一个对象
    private AbstractHandler nextHandler;

    public void setNextHandler(AbstractHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handle(Object request, Object response) {
        doHandle(request, response);
        if (this.getNextHandler() != null) {
            this.getNextHandler().handle(request, response);
        }
    }

    // 具体参数拦截实现, 交给子类去完成
    protected abstract void doHandle(Object request, Object response);

    public AbstractHandler getNextHandler() {
        return nextHandler;
    }
}
