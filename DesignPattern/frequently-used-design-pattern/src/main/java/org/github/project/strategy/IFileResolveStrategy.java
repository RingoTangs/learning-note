package org.github.project.strategy;

/**
 * 策略模式：定义了算法族, 分别封装起来, 让它们之间可以相互替换。
 *
 * @auther yang
 * @date 2022/1/21 12:27
 */
public interface IFileResolveStrategy {

    /**
     * 获取文件类型
     */
    FileTypeEnum getFileType();

    /**
     * 解析文件
     */
    void resolve(Object param);
}
