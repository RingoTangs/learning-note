package org.github.project.observer;

/**
 * 消息服务
 */
public interface IMessageService {

    /**
     * 发送消息
     */
    void send(String msg);

}
