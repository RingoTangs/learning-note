const p1 = new Promise((resolve, reject) => resolve({ number: 1 }));
const p2 = new Promise((resolve, reject) => resolve({ number: 2 }));
const p3 = new Promise((resolve, reject) => resolve({ number: 3 }));
const p = Promise.all([p1, p2, p3]);
console.log(p);
// 返回的是一个成功的数组 
p.then(ret => {
    console.log(ret);   // [ { number: 1 }, { number: 2 }, { number: 3 } ]
    let sum = 0;
    ret.forEach((cur, index, arr) => sum += cur.number);
    console.log('sum =', sum);
    return sum;
})
    .then(ret => console.log(ret))
    .catch(err => console.log(err));
