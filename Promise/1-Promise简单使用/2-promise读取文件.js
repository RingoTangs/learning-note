const { log } = require('console');
const fs = require('fs');

// 方式一：回调函数的形式
// #region
// fs.readFile('./resource/1.txt', (err, data) => {
//     // 如果出错, 抛出错误
//     if (err) throw err;
//     // 输出文件内容
//     console.log(data.toString());
// });
// #endregion

// 方式二：Promise的形式
const p = new Promise((resolve, reject) => {
    fs.readFile('./resource/1.txt', (err, data) => {
        if (err)
            reject(err);
        else
            resolve({ data });
    });
});

p.then(res => {
    console.log(res.data.toString());
}, err => {
    console.log(err);
});