

# 一、Promise的理解和使用

## 1.1. Promise是什么？

Promise 是 JavaScript 中进行**异步编程的新解决方案**。旧的方案单纯使用回调函数。

**异步编程**：

- `node fs` 文件操作。

  ```javascript
  require('fs').readFile('./index.txt', (err, data) => {
      if (err) {
          return console.error(err);
      }
      console.log("异步读取: " + data.toString());
  });
  ```

- 数据库操作。

- `AJAX`。

  ```javascript
  $.get('/api', data => {});
  ```

- 定时器。

  ```javascript
  setTimeout(() => {}, 2000);
  ```

  

## 1.2. 为什么用Promise？

1. **支持链式调用，可以解决回调地狱问题**。
2. 指定回调函数的方式更加灵活。

![](https://cdn.jsdelivr.net/gh/RingoTangs/image-hosting@master/Promise/image.4bxqsrv3pa20.png)

## 1.3. Promise简单使用

```javascript
// resolve: 解决 函数类型的数据。
// reject: 拒绝 函数类型的数据。
const p = new Promise((resolve, reject) => {
    // ...
    if (express == true) 
        resolve({ state: true });
    else 
        reject({ state: false });
});

// 调用 then() 函数
// param1: 接收 resolve() 传过来的数据。
// param2: 接收 reject() 传过来的数据。
p.then(ret => { }, err => { });
```

