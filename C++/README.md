# 一、数据类型

## 1.1 整型

| 数据类型  | 占用空间                                         | 取值范围           |
| --------- | ------------------------------------------------ | ------------------ |
| short     | 2 byte                                           | (-2^15 ~ 2^15-1)   |
| int       | 4 byte                                           | (-2^31 ~ 2^31 - 1) |
| long      | windows 4 byte, linux 4 byte(32位), 8 byte(32位) | (-2^31 ~ 2^31 - 1) |
| long long | 8 byte                                           | (-2^63 ~ 2^63 - 1) |



## 1.2 sizeof关键字

`sizeof`是C语言的32个关键字之一，并非“函数”(我们会后面介绍)，也叫长度(求字节)运算符, `sizeof`是一种单目运算符，以字节为单位返回某操作数的大小，用来求某一类型变量的长度。其运算对象可以是任何数据类型或变量。

```c++
#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {
	short a = 1;
	int b = 1;
	long c = 1;
	long long d = 1;

	cout << "sizeof(a) = " << sizeof(a) << endl;  // 2
	cout << "sizeof(b) = " << sizeof(b) << endl;  // 4
	cout << "sizeof(c) = " << sizeof(c) << endl;  // 4
	cout << "sizeof(d) = " << sizeof(d) << endl;  // 8

	cout << "sizeof(int) = " << sizeof(int) << endl; // 4

	int *p = (int *)malloc(sizeof(int));
	*p = 10;
	cout << "p = " << p << endl;					// 0x721680
	cout << "*p = " << *p << endl;					// 10
	cout << "sizeof(p) = " << sizeof(p) << endl;    // 8
	cout << "sizeof(*p) = " << sizeof(*p) << endl;	// 4

	return 0;
}
```

## 1.3 浮点型

| 数据类型 | 占用空间 | 有效数字范围    |
| -------- | -------- | --------------- |
| float    | 4字节    | 7位有效数字     |
| double   | 8字节    | 15~16位有效数字 |


```c++
#include <iostream>

using namespace std;

int main() {
	// float 单精度要在后边加上 "F/f"
	float f1 = 3.141592653F;

	// 3.14 默认就是 double 双精度
	double d1 = 3.141592653;

	// 默认情况下, 输出一个小数, 会显示6位有效数字
	cout << f1 << endl;		// 3.14159
	cout << d1 << endl;		// 3.14159

	cout << "sizeof(float) = " << sizeof(float) << endl;	// 4
	cout << "sizeof(double) = " << sizeof(double) << endl;  // 8

	// 科学计数法(用的少)
	float f2 = 3e4;
	float f3 = 3e-4;

	cout << "f2 = " << f2 << endl;	// 30000
	cout << "f3 = " << f3 << endl;  // 0.0003

	return 0;
}
```



## 1.4 字符型

- `C、C++` 中字符型变量只占用 1 个字节。

- 字符型变量是将对应的 ASCII 编码放入到内存中。

```c++
#include <iostream>

using namespace std;

int main() {
	cout << "sizeof(char) = " << sizeof(char) << endl;   // 1

	char a = 'a', A = 'A';
	cout << "a = " << (int)a << ", A = " << (int)A << endl; // a = 97, A = 65
	cout << 'a' - 'A';	// 32

	return 0;
}
```



## 1.5 字符串类型

```c++
#include <iostream>
#include <string>

using namespace std;

int main() {
	// 1: C语言风格
	char s1[] = "Hello World";
	cout << "s1: " << s1 << endl;	// s1: Hello World

	const char *s2 = "Hello World";
	cout << "s2: " << s2 << endl;	// s2: Hello World
	cout << "*s2: " << *s2 << endl; // *s2: H

	// 2: C++ 风格(使用字符串需要引入头文件 <string>)
	string s3 = "Hello World";
	cout << "s3: " << s3 << endl;	// s3: Hello World

	return 0;
}
```

## 1.6 布尔类型

```c++
#include <iostream>

using namespace std;

int main() {
	// true 是 1, false 是 0.
	bool a = true, b = false;

	cout << "a = " << a << endl; // a = 1
	cout << "b = " << b << endl; // b = 0

	cout << "sizeof(bool) = " << sizeof(bool) << endl; // 1

	return 0;
}
```

## 1.7 数据的输入

关键字：`cin`。

语法：`cin >> varies`.



