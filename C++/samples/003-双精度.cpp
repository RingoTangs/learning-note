#include <iostream>

using namespace std;

int main() {
	// float 单精度要在后边加上 "F/f"
	float f1 = 3.141592653F;

	// 3.14 默认就是 double 双精度
	double d1 = 3.141592653;

	// 默认情况下, 输出一个小数, 会显示6位有效数字
	cout << f1 << endl;		// 3.14159
	cout << d1 << endl;		// 3.14159

	cout << "sizeof(float) = " << sizeof(float) << endl;	// 4
	cout << "sizeof(double) = " << sizeof(double) << endl;  // 8

	// 科学计数法(用的少)
	float f2 = 3e4;
	float f3 = 3e-4;

	cout << "f2 = " << f2 << endl;	// 30000
	cout << "f3 = " << f3 << endl;  // 0.0003

	return 0;
}