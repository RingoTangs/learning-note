#include <iostream>

using namespace std;

bool contains7(int);

int main() {
	for (int i = 1; i <= 100; ++i) {
		if (contains7(i)) {
			cout << i << endl;
		}
	}
	return 0;
}

/**
* 1-100的数字, 如果个位/十位含有数字7 或者是7的倍数, 输出该数
*/
bool contains7(int n) {
	if (!(n % 7))
		return true;
	int temp = n;
	while (temp > 0) {
		if (temp % 10 == 7)
			return true;
		temp /= 10;
	}
	return false;
}