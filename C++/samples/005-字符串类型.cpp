#include <iostream>
#include <string>

using namespace std;

int main() {
	// 1: C语言风格
	char s1[] = "Hello World";
	cout << "s1: " << s1 << endl;	// s1: Hello World

	const char *s2 = "Hello World";
	cout << "s2: " << s2 << endl;	// s2: Hello World
	cout << "*s2: " << *s2 << endl; // *s2: H

	// 2: C++ 风格(使用字符串需要引入头文件 <string>)
	string s3 = "Hello World";
	cout << "s3: " << s3 << endl;	// s3: Hello World

	return 0;
}