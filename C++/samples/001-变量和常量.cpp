#include <iostream>

// 1. 定义宏常量
#define day 7

using namespace std;

int main() {
	// 2. 使用 const 修饰变量
	const char *name = "zs";
	cout << "day: " << day << "\tname: " << name << endl;
	return 0;
}
