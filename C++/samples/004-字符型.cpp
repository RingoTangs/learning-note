#include <iostream>

using namespace std;

int main() {
	cout << "sizeof(char) = " << sizeof(char) << endl;   // 1

	char a = 'a', A = 'A';
	cout << "a = " << (int)a << ", A = " << (int)A << endl; // a = 97, A = 65
	cout << 'a' - 'A';	// 32

	return 0;
}