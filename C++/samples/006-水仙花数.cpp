#include <iostream>
#include <math.h>

using namespace std;

bool isNarcissus(int);

int main() {
	for (int i = 100; i < 1000; ++i) {
		if (isNarcissus(i))
			cout << i << endl;
	}
	return 0;
}

bool isNarcissus(int num) {
	int sum = 0, n = num;
	while (n > 0) {
		int a = n % 10;  // ����
		n /= 10;
		sum += pow(a, 3);
	}
	if (sum == num)
		return true;
	return false;
}
