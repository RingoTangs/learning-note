#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {
	short a = 1;
	int b = 1;
	long c = 1;
	long long d = 1;

	cout << "sizeof(a) = " << sizeof(a) << endl;  // 2
	cout << "sizeof(b) = " << sizeof(b) << endl;  // 4
	cout << "sizeof(c) = " << sizeof(c) << endl;  // 4
	cout << "sizeof(d) = " << sizeof(d) << endl;  // 8

	cout << "sizeof(int) = " << sizeof(int) << endl; // 4

	int *p = (int *)malloc(sizeof(int));
	*p = 10;
	cout << "p = " << p << endl;					// 0x721680
	cout << "*p = " << *p << endl;					// 10
	cout << "sizeof(p) = " << sizeof(p) << endl;    // 8
	cout << "sizeof(*p) = " << sizeof(*p) << endl;	// 4

	return 0;
}