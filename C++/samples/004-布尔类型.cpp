#include <iostream>

using namespace std;

int main() {
	// true �� 1, false �� 0.
	bool a = true, b = false;

	cout << "a = " << a << endl; // a = 1
	cout << "b = " << b << endl; // b = 0

	cout << "sizeof(bool) = " << sizeof(bool) << endl; // 1

	return 0;
}
