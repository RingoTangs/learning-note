# 一、webpack基础

## 1. CLI(Command Line Interface)

1. 运行指令：

   ```shell
   # 文件未压缩
   webpack ./src/main.js -o ./dist --mode=development
   # 文件已压缩
   webpack ./src/main.js -o ./dist --mode=production
   ```

2. 结论：

   - webpack 可以编译打包 `js、json` 文件。
   - webpack 能将 ES6 的模块化语法转换成浏览器能够识别的语法。
   - 可以压缩代码。

3. 缺点：

   - webpack 不能编译打包 `css、img` 等文件。
   - 不能将 ES6 基本语法转化为 ES5 以下语法。

4. 改善：使用 webpack 配置文件解决，自定义功能。

## 2. webpack配置文件

在项目的路径下添加 `webpack.config.js`。配置文件的入口和输出。

```javascript
const path = require('path')

module.exports = {
  mode: 'development',
  // entry 完整写法
  // entry: {
  //   main: './src/main.js',
  // },
  entry: './src/main.js', // 入口
  output: {
    path: path.resolve(__dirname, 'dist/js'), // 在当前目录下添加 dist 文件夹
    filename: 'bundle.js',
  },
}
```



## 3. 解析less文件

1. 安装 loader：`npm install css-loader style-loader less-loader less --save-dev`。

2. `webpack.config.js` 中配置 loader

   ```javascript
   // webpack.config.js
   module.exports = {
     module: {
       rules: [
         // 解析less(没生成单独的css文件)
         {
           test: /\.less$/, // 匹配所有的 .less 文件
           use: [
             'style-loader', // 用于在 HTML 文档中创建一个 style 标签, 将样式塞进去
             'css-loader', // 将 less 编译后的 css 转换成 CommonJS 的一个模块
             'less-loader', // 将 less 编译为 css, 但是不生成单独的 css 文件, 存在内存中。
           ],
         },
       ],
     },
   }

   ```
   

## 4. js语法检查



```shell
npm install webpack@4.46.0 -g
npm install webpack-cli@3.3.12 -g
```

```shell
npm install webpack@4.46.0 --save-dev
npm install webpack-cli@3.3.12 --save-dev
```

