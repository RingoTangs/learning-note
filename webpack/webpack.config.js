const path = require('path')

module.exports = {
  mode: 'development',
  // entry 完整写法
  // entry: {
  //   main: './src/main.js',
  // },
  entry: './src/main.js', // 入口
  output: {
    path: path.resolve(__dirname, 'dist/js'), // 在当前目录下添加 dist 文件夹
    filename: 'bundle.js',
  },
  module: {
    rules: [
      // 解析less(没生成单独的css文件)
      {
        test: /\.less$/, // 匹配所有的 .less 文件
        use: [
          'style-loader', // 用于在 HTML 文档中创建一个 style 标签, 将样式塞进去
          'css-loader', // 将 less 编译后的 css 转换成 CommonJS 的一个模块
          'less-loader', // 将 less 编译为 css, 但是不生成单独的 css 文件, 存在内存中。
        ],
      },
      // {
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   enforce: 'pre',
      //   use: {
      //     loader: 'eslint-loader',
      //   },
      // },
    ],
  },
}
